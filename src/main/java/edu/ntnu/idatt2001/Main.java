package edu.ntnu.idatt2001;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;

import java.io.FileInputStream;
import java.util.Optional;

/**
 * Class that holds the GUI and main method which runs the javafx Application
 */
public class Main extends Application{
    //TableView<Patient> tableView = new TableView<Patient>();


    @Override
    public void start(Stage primaryStage) {

        GuiElementFactory factory = new GuiElementFactory();
        TableView tableView = factory.createTable();

        BorderPane root = new BorderPane();
        VBox vBox = new VBox();

        MenuBar mainMenu = factory.createMenus();
        ToolBar toolBar = factory.createToolBars();

        vBox.getChildren().add(mainMenu);
        vBox.getChildren().add(toolBar);
        root.setTop(vBox);
        root.setCenter(tableView);

        Scene scene = new Scene(root, 500, 350);

        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
