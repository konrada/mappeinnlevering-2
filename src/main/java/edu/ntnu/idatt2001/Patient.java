package edu.ntnu.idatt2001;

/**
 * Class to represent Patient
 */
public class Patient {
    private String socialSecurityNumber = null;
    private String firstName = null;
    private String lastName = null;
    private String diagnosis = null;
    private String generalPractitioner = null;

    /**
     * Constructor for creating patient
     * @param socialSecurityNumber patients social security number
     * @param firstName patients first name
     * @param lastName patients last name
     * @param diagnosis patients diagnosis
     * @param generalPractitioner patients general practitioner
     */
    public Patient(String socialSecurityNumber, String firstName,
                   String lastName, String diagnosis, String generalPractitioner) {
        this.socialSecurityNumber = socialSecurityNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.diagnosis = diagnosis;
        this.generalPractitioner = generalPractitioner;
    }
    public String getLastName() {
        return lastName;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public String getGeneralPractitioner() {
        return generalPractitioner;
    }
    public String toString() {
        return socialSecurityNumber + ";" + firstName + ";" + lastName + ";" + diagnosis + ";" + generalPractitioner;
    }
}
