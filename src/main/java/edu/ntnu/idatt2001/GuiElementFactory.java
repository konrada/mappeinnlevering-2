package edu.ntnu.idatt2001;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Optional;

/**
 * Class that creates the GUI elements for the application
 */
public class GuiElementFactory {
    private TableView tableView;

    /**
     * Method that creates a tableview which represents the patient register
     * @return tableview used to represent the patient register
     */
    public TableView createTable(){
        tableView = new TableView<Patient>();
        TableColumn<Patient, String> column1 = new TableColumn<>("Social Security Number");
        column1.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));

        TableColumn<Patient, String> column2 = new TableColumn<>("First Name");
        column2.setCellValueFactory(new PropertyValueFactory<>("firstName"));

        TableColumn<Patient, String> column3 = new TableColumn<>("Last Name");
        column3.setCellValueFactory(new PropertyValueFactory<>("lastName"));

        TableColumn<Patient, String> column4 = new TableColumn<>("Diagnosis");
        column4.setCellValueFactory(new PropertyValueFactory<>("diagnosis"));

        TableColumn<Patient, String> column5 = new TableColumn<>("General Practitioner");
        column5.setCellValueFactory(new PropertyValueFactory<>("generalPractitioner"));

        tableView.getColumns().add(column1);
        tableView.getColumns().add(column2);
        tableView.getColumns().add(column3);
        tableView.getColumns().add(column4);
        tableView.getColumns().add(column5);
        return tableView;
    }


    /**
     * Method that creates a new GUI dialog to be used to add a new patient to the register
     * @return eventHandler which can then be assigned to a button in the GUI
     */
    private EventHandler<ActionEvent> addPatient() {
        EventHandler<ActionEvent> eventHandler = actionEvent -> {
            Dialog<String> dialog = new Dialog();
            dialog.setTitle("Add patient");
            dialog.setHeaderText("Enter details");
            GridPane gp = new GridPane();
            gp.setHgap(10);
            gp.setVgap(10);
            gp.setPadding(new Insets(20, 150, 10, 10));
            Button add = new Button("Add");
            ButtonType cancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
            dialog.getDialogPane().getButtonTypes().add(cancel);
            TextField firstNameField = new TextField();
            firstNameField.setPromptText("First name");
            TextField lastNameField = new TextField();
            lastNameField.setPromptText("Last name");
            TextField numberField = new TextField();
            numberField.setPromptText("Social security number");
            gp.add(new Label("First name"), 0, 0);
            gp.add(firstNameField, 1, 0);
            gp.add(new Label("Last name"), 0, 1);
            gp.add(lastNameField, 1, 1);
            gp.add(new Label("Social security number"), 0, 2);
            gp.add(numberField, 1, 2);
            gp.add(add, 1, 3);
            dialog.getDialogPane().setContent(gp);

            EventHandler<ActionEvent> eventHandler3 = actionEvent2 -> {
                Patient patient = new Patient(numberField.getText(), firstNameField.getText(), lastNameField.getText(), "", "");
                tableView.getItems().add(patient);
                dialog.close();
            };
            add.setOnAction(eventHandler3);
            dialog.showAndWait();
        };
        return eventHandler;
    }

    /**
     * Method that creates a new GUI dialog to be used to edit a patient in the register
     * @return eventHandler which can be assigned to a button in the GUI
     */
    private EventHandler<ActionEvent> editPatient() {
        EventHandler<ActionEvent> eventHandler = actionEvent -> {
            Dialog<String> dialog = new Dialog();
            dialog.setTitle("Edit patient");
            dialog.setHeaderText("Enter details");
            GridPane gp = new GridPane();
            gp.setHgap(10);
            gp.setVgap(10);
            gp.setPadding(new Insets(20, 150, 10, 10));
            Button edit = new Button("Edit");
            ButtonType cancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
            dialog.getDialogPane().getButtonTypes().add(cancel);
            TextField firstNameField = new TextField();
            Patient patient = (Patient) tableView.getSelectionModel().getSelectedItem();
            firstNameField.setPromptText(patient.getFirstName());
            TextField lastNameField = new TextField();
            lastNameField.setPromptText(patient.getLastName());
            TextField numberField = new TextField();
            numberField.setPromptText(patient.getSocialSecurityNumber());
            gp.add(new Label("First name"), 0, 0);
            gp.add(firstNameField, 1, 0);
            gp.add(new Label("Last name"), 0, 1);
            gp.add(lastNameField, 1, 1);
            gp.add(new Label("Social security number"), 0, 2);
            gp.add(numberField, 1, 2);
            gp.add(edit, 1, 3);
            dialog.getDialogPane().setContent(gp);
            EventHandler<ActionEvent> eventHandler1 = actionEvent1 -> {
                Patient patient1 = new Patient(numberField.getText(), firstNameField.getText(), lastNameField.getText(), "", "");
                int selectedIndex = tableView.getSelectionModel().getSelectedIndex();
                tableView.getItems().set(selectedIndex, patient1);
                dialog.close();

            };
            edit.setOnAction(eventHandler1);
            dialog.showAndWait();
        };
        return eventHandler;
    }

    /**
     * Method that creates a new GUI dialog to be used to delete a patient in the register
     * @return eventHandler which can be assigned to a button in the GUI
     */
    private EventHandler<ActionEvent> deletePatient() {
        EventHandler<ActionEvent> eventHandler = actionEvent -> {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Delete Patient");
            alert.setContentText("Are you sure you want to delete?");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                int selectedIndex = tableView.getSelectionModel().getSelectedIndex();
                if (selectedIndex >= 0) {
                    tableView.getItems().remove(selectedIndex);
                } else {
                    Alert alert1 = new Alert(Alert.AlertType.WARNING);
                    alert1.setTitle("No patient selected");
                    alert1.setHeaderText("No patient selected");
                    alert1.setContentText("Please select a patient from the list");
                    alert1.showAndWait();
                }
            }
        };
        return eventHandler;
    }

    private EventHandler<ActionEvent> exportCSV() {
        EventHandler<ActionEvent> eventHandler = actionEvent -> {
            Dialog<String> dialog = new Dialog();
            dialog.setTitle("Export");
            dialog.setHeaderText("Enter filename and path");
            GridPane gp = new GridPane();
            gp.setHgap(10);
            gp.setVgap(10);
            gp.setPadding(new Insets(20, 150, 10, 10));
            ButtonType cancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
            dialog.getDialogPane().getButtonTypes().add(cancel);
            Button export = new Button("Export");
            TextField fileNameField = new TextField();
            fileNameField.setPromptText("Filename");
            TextField pathField = new TextField();
            pathField.setPromptText("File path");
            gp.add(new Label("Filename"), 0, 0);
            gp.add(fileNameField, 1, 0);
            gp.add(new Label("File path"), 0, 1);
            gp.add(pathField, 1, 1);
            gp.add(export, 1, 2);
            dialog.getDialogPane().setContent(gp);


            EventHandler<ActionEvent> eventHandler1 = actionEvent1 -> {
                FileWriter writer = null;
                String filepath = pathField.getText();
                try {
                    writer = new FileWriter(filepath);
                    for (int i = 0; i < tableView.getItems().size();i++) {
                        writer.write(tableView.getItems().get(i).toString());
                    }
                    } catch (IOException e) {
                    System.err.println(e.getMessage());
                } finally {
                    try {
                        writer.close();
                    } catch (IOException e) {
                        System.err.println(e.getMessage());
                    }
                }
                dialog.close();
            };
            export.setOnAction(eventHandler1);
            dialog.showAndWait();
        };
        return eventHandler;
    }



    /**
     * Method that creates menus for the GUI
     * @return MenuBar
     */
    public MenuBar createMenus() {
        MenuBar menuBar = new MenuBar();

        Menu fileMenu = new Menu("File");
        MenuItem importCSV = new MenuItem("Import from .CSV");
        //importCSV.setOnAction(importFile());
        MenuItem exportCSV = new MenuItem("Export to .CSV");
        exportCSV.setOnAction(exportCSV());
        fileMenu.getItems().add(importCSV);
        fileMenu.getItems().add(exportCSV);
        Menu editMenu = new Menu("Edit");
        MenuItem addPatient = new MenuItem("Add new patient");
        addPatient.setOnAction(addPatient());

        MenuItem editPatient = new MenuItem("Edit selected patient");
        editPatient.setOnAction(editPatient());
        MenuItem removePatient = new MenuItem("Remove selected patient");
        removePatient.setOnAction(deletePatient());
        editMenu.getItems().add(addPatient);
        editMenu.getItems().add(removePatient);
        editMenu.getItems().add(editPatient);
        Menu helpMenu = new Menu("Help");
        MenuItem about = new MenuItem("About");
        EventHandler<ActionEvent> eventHandler = actionEvent -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("About");
            alert.setHeaderText("Patients register \n v0.1-SNAPSHOT");
            alert.setContentText("Application created by Konrad Åsødegård \n 2021");
            alert.showAndWait();
        };
        about.setOnAction(eventHandler);
        helpMenu.getItems().add(about);

        menuBar.getMenus().addAll(fileMenu, editMenu, helpMenu);

        return menuBar;

    }

    /**
     * Method that creates a toolbar for the GUI
     * @return ToolBar
     */
    public ToolBar createToolBars() {
        ToolBar toolBar = new ToolBar();

        Button addPatient = new Button();
        ImageView addPatientImage = new ImageView(new Image("add.png"));
        addPatientImage.setFitHeight(50);
        addPatientImage.setFitWidth(50);
        addPatient.setGraphic(addPatientImage);
        addPatient.setOnAction(addPatient());

        Button deletePatient = new Button();
        ImageView deletePatientImage = new ImageView(new Image("delete.png"));
        deletePatientImage.setFitWidth(50);
        deletePatientImage.setFitHeight(50);
        deletePatient.setGraphic(deletePatientImage);
        deletePatient.setOnAction(deletePatient());

        Button editPatient = new Button();
        ImageView editPatientImage = new ImageView(new Image("edit.png"));
        editPatientImage.setFitHeight(50);
        editPatientImage.setFitWidth(50);
        editPatient.setGraphic(editPatientImage);
        editPatient.setOnAction(editPatient());

        toolBar.getItems().addAll(addPatient, deletePatient, editPatient);
        return toolBar;

    }
}
